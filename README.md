
Forked from : https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/multilepton/hhmlfw2/-/tree/b_BDT
# Getting started <a name="getting-started"></a>

## Setup <a name="setup"></a>

The setup procedure is simple. Clone the package and then source the setup script for the first time. Please follow these:

```
git clone https://:@gitlab.cern.ch:8443/atlasHTop/ttHMultiGFW2.git
cd ttHMultiGFW2
source setup.sh
```


## Running locally <a name="running-locally"></a>


```
python gfw2.py <input.txt> <output.root>  <total_weights> <xs>
```
Where:
*  <input.txt>: is the list of input ROOT files (xROOTD file URL lists are also acceptable)
*  <output.root> : name of your output root file
*  <total_weights>:  This is the sum of the events weights used for the normalization. User is responsible for providing this (grid job submission script does this automatically for you)
   <xs> : This is the mc_XS x k-Factor value that you want to use to normalize the data. User is responsible for providing this (grid job submision script does this automatically for you)
Example:
```
python gfw2.py 410470_Files.txt output.root 1.09034066026e+11 452.352426
```

### SumOfEventWeight calculation <a name="sumOfEventWeight-calclation"></a>

By default, the GFW2 package doesn't save the sumOfEights tree in the mini-ntuples. Instead, the necessary SumOfEventWeight calculation for normalization happens "on-the-fly" when submitting GFW2 jobs to the grid or lxplus batch. The ```gfw2.py``` script will probably work anyway out-of-the-box which, for the purposes of seeing if the code runs fine and do some debugging, is more than enough but if you absolutely need the test file to have the correct normalization you need to manually calculate the SumOfEventWeight.



## Running on the Grid <a name="running-on-lxplus-grid"></a>

This is an example running jobs on the Grid.

Please follow these commands to submit Grid jobs.
```
python subGrid.py --inDsTxt <inputGFW1List> --nickname <yourUserID> --id_text <submissionIdentifier>
```

Where:

* --inDsTxt: Is the list (.txt file) containing all the GFW1 datasets that you wish to submit (stored in ```ttHMultilepton/share/```).
* --id_text: Is the unique identifier for your whole submission. This is very important to later on download/monitor your jobs using rucio/panda. The submission script will automatically attach the current date in the format "YYMMDD" to the identifier.
* --nickname: Is typically your grid user ID. If you have production role then use that (e.g. "phys-higgs") as your nickname instead.

Note: the SumOfEventWeight calculation will be taken care of automatically by the submission script.

Example:
```
python subGrid.py --inDsTxt mc16d.txt --nickname phys-higgs --id_text mc16d_v2
```

## BDT implementation <a name="useful-scripts"></a>
code gfw2_bdt.py has a simple way to run BDTs on gn2.


### Any Comments and Questions are welcome !!
