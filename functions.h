using VecI = const ROOT::RVec<int>&;
using VecF = const ROOT::RVec<float>&;
using VecD = const ROOT::RVec<double>&;

/**
 * Get leading jet pt
 */
float getJetPt0(VecF jets_Pt)
{
  auto rSortJetPt = Sort(jets_Pt);//ascending order
  auto rReversePt = Reverse(rSortJetPt);
  if (rSortJetPt.size()>0) {return rReversePt.at(0);}
  else return -1.0;
}
float getJetPt1(VecF jets_Pt)
{
  auto rSortJetPt = Sort(jets_Pt);//ascending order
  auto rReversePt = Reverse(rSortJetPt);
  if(rSortJetPt.size()>1) {return rReversePt.at(1);}
  else return -1.0;
}
float getJetPt2(VecF jets_Pt)
{
  auto rSortJetPt = Sort(jets_Pt);//ascending order
  auto rReversePt = Reverse(rSortJetPt);
  if(rSortJetPt.size()>2) {return rReversePt.at(2);}
  else return -1.0;
}

float getJetEta0(VecF jets_Pt, VecF jets_Eta)
{
    auto rSortJetPtId = Reverse(Argsort(jets_Pt));
    auto jetEta = -99.;
    if(rSortJetPtId.size()>0) {
       jetEta = jets_Eta.at(rSortJetPtId.at(0));
    }
    return jetEta;
}

float getJetEta1(VecF jets_Pt, VecF jets_Eta)
{
    auto rSortJetPtId = Reverse(Argsort(jets_Pt));
    auto jetEta     = -99.;
    if(rSortJetPtId.size()>1)
    {
       jetEta = jets_Eta.at(rSortJetPtId.at(1));
     }
     return jetEta;
}

float getJetEta2(VecF jets_Pt, VecF jets_Eta)
{
    auto rSortJetPtId = Reverse(Argsort(jets_Pt));
    auto jetEta     = -99.;
    if(rSortJetPtId.size()>2)
    {
       jetEta = jets_Eta.at(rSortJetPtId.at(2));
     }
     return jetEta;
}

/** 
 * Get the minimum DeltaR between leptons and jets
 */
float getMindr(VecF phi, VecF eta, VecF pt, float lep_phi_0, float lep_eta_0, float lep_pt_0, float lep_phi_1, float lep_eta_1, float lep_pt_1, float lep_phi_2, float lep_eta_2, float lep_pt_2, int trilep_type)
{
    float minDeltaR_LJ_0     = 9999;
    float minDeltaR_LJ_1     = 9999;
    float minDeltaR_LJ_2     = 9999;
    float DeltaR_min_lep_jet = 9999;
    TVector3 jet;
    TVector3 lep0;
    TVector3 lep1;
    TVector3 lep2;
    lep0.SetPtEtaPhi(lep_pt_0, lep_eta_0, lep_phi_0);
    lep1.SetPtEtaPhi(lep_pt_1, lep_eta_1, lep_phi_1);
    lep2.SetPtEtaPhi(lep_pt_2, lep_eta_2, lep_phi_2);
    for (size_t i = 0; i < phi.size(); i++) {
        jet.SetPtEtaPhi(pt[i], eta[i], phi[i]);
        float  DR_LJ_0_tmp  = ROOT::Math::VectorUtil::DeltaR(lep0,jet);
        if (DR_LJ_0_tmp < minDeltaR_LJ_0) {
            minDeltaR_LJ_0 = DR_LJ_0_tmp;
        }
    
        float  DR_LJ_1_tmp  = ROOT::Math::VectorUtil::DeltaR(lep1,jet);
        if (DR_LJ_1_tmp < minDeltaR_LJ_1) {
            minDeltaR_LJ_1 = DR_LJ_1_tmp;
        }
        float  DR_LJ_2_tmp  = ROOT::Math::VectorUtil::DeltaR(lep2,jet);
        if (DR_LJ_2_tmp < minDeltaR_LJ_2) {
            minDeltaR_LJ_2 = DR_LJ_2_tmp;
        }
    
    }
    DeltaR_min_lep_jet = minDeltaR_LJ_0;
    if (minDeltaR_LJ_1 < DeltaR_min_lep_jet) DeltaR_min_lep_jet = minDeltaR_LJ_1;
    if (trilep_type && (minDeltaR_LJ_2 < DeltaR_min_lep_jet)) DeltaR_min_lep_jet = minDeltaR_LJ_2;
    return DeltaR_min_lep_jet;
}

/**
 * get lepton-bjet minimum DeltaR
 */
float getMindrBjet(VecF phi, VecF eta, VecF pt, VecI bTag, float lep_phi_0, float lep_eta_0, float lep_pt_0, float lep_phi_1, float lep_eta_1, float lep_pt_1, float lep_phi_2, float lep_eta_2, float lep_pt_2, int trilep_type)
{
    float minDeltaR_LJ_0     = 9999;
    float minDeltaR_LJ_1     = 9999;
    float minDeltaR_LJ_2     = 9999;
    float DeltaR_min_lep_jet = 9999;
    TVector3 jet;
    TVector3 lep0;
    TVector3 lep1;
    TVector3 lep2;
    lep0.SetPtEtaPhi(lep_pt_0, lep_eta_0, lep_phi_0);
    lep1.SetPtEtaPhi(lep_pt_1, lep_eta_1, lep_phi_1);
    lep2.SetPtEtaPhi(lep_pt_2, lep_eta_2, lep_phi_2);
    for (size_t i = 0; i < phi.size(); i++) {
        if (bTag.at(i))
        {
            jet.SetPtEtaPhi(pt[i], eta[i], phi[i]);
            float  DR_LJ_0_tmp  = ROOT::Math::VectorUtil::DeltaR(lep0,jet);
            if (DR_LJ_0_tmp < minDeltaR_LJ_0) {
                minDeltaR_LJ_0 = DR_LJ_0_tmp;
            }
    
            float  DR_LJ_1_tmp  = ROOT::Math::VectorUtil::DeltaR(lep1,jet);
            if (DR_LJ_1_tmp < minDeltaR_LJ_1) {
                minDeltaR_LJ_1 = DR_LJ_1_tmp;
            }
            float  DR_LJ_2_tmp  = ROOT::Math::VectorUtil::DeltaR(lep2,jet);
            if (DR_LJ_2_tmp < minDeltaR_LJ_2) {
                minDeltaR_LJ_2 = DR_LJ_2_tmp;
            }
        }
    }
    DeltaR_min_lep_jet = minDeltaR_LJ_0;
    if (minDeltaR_LJ_1 < DeltaR_min_lep_jet) DeltaR_min_lep_jet = minDeltaR_LJ_1;
    if (trilep_type && (minDeltaR_LJ_2 < DeltaR_min_lep_jet)) DeltaR_min_lep_jet = minDeltaR_LJ_2;
    return DeltaR_min_lep_jet;
}


/**Get minimum deltaR between lepton_0 and jet**/
float getMindr0(VecF phi, VecF eta, VecF pt, float lep_phi_0, float lep_eta_0, float lep_pt_0)
{
    float minDeltaR_LJ_0     = 9999;
    TVector3 jet;
    TVector3 lep;
    lep.SetPtEtaPhi(lep_pt_0, lep_eta_0, lep_phi_0);
    for (size_t i = 0; i < phi.size(); i++) {
        jet.SetPtEtaPhi(pt[i], eta[i], phi[i]);
        float  DR_LJ_0_tmp  = ROOT::Math::VectorUtil::DeltaR(lep,jet);
        if (DR_LJ_0_tmp < minDeltaR_LJ_0) {
            minDeltaR_LJ_0 = DR_LJ_0_tmp;
        }
    
    }
    return minDeltaR_LJ_0;
}


/**Get Magnitude of leptons and MET**/
float getMLepMet(float lep_E_0, float lep_Phi_0, float lep_Eta_0, float lep_Pt_0, float lep_E_1, float lep_Phi_1, float lep_Eta_1, float lep_Pt_1, float met_met, float met_phi)
{
    TLorentzVector lep0, lep1, lep2, lep3, lep4, Met;
    lep0.SetPtEtaPhiE(lep_Pt_0, lep_Eta_0, lep_Phi_0, lep_E_0);
    lep1.SetPtEtaPhiE(lep_Pt_1, lep_Eta_1, lep_Phi_1, lep_E_1);
    Met.SetPtEtaPhiM(met_met, 0, met_phi, 0);

  float MLepMet = (lep0+lep1+Met).M(); // sqrt(E^2-Px^2-Py^2-Pz^2)
  return MLepMet; 
}


/**Get MT of leptons and MET**/
float getMtLepMet(float lep_E_0, float lep_Phi_0, float lep_Eta_0, float lep_Pt_0, float lep_E_1, float lep_Phi_1, float lep_Eta_1, float lep_Pt_1, float met_met, float met_phi)
{
    TLorentzVector lep0, lep1, lep2, lep3, lep4, Met;
    lep0.SetPtEtaPhiE(lep_Pt_0, lep_Eta_0, lep_Phi_0, lep_E_0);
    lep1.SetPtEtaPhiE(lep_Pt_1, lep_Eta_1, lep_Phi_1, lep_E_1);
    Met.SetPtEtaPhiM(met_met, 0, met_phi, 0);

  float MtLepMet = (lep0+lep1+Met).Mt(); // sqrt(E^2-Pz^2)
  return MtLepMet; 
}

/**Find the highest Mjj with the most forward jet**/
auto frwdJetPairVars(VecF jet_pt, VecF jet_eta, VecF jet_phi, VecF jet_e)
{
    float maxJetEta =-10;
    float maxMjj    =-10;
    float frwdjetEta= -10;
    float maxmjjJetdEta =-10;
    TLorentzVector frwdjet;
    auto max_mjjJet = TLorentzVector();
    ROOT::RVec<TLorentzVector> jet_lv;
    for(size_t i =0 ; i < jet_pt.size(); ++i)
    {
        TLorentzVector lv;
        lv.SetPtEtaPhiE(jet_pt.at(i),jet_eta.at(i),jet_phi.at(i),jet_e.at(i));
        jet_lv.push_back(lv);
    }
    for (auto j: jet_lv)
    {
        if (fabs(j.Eta()) > maxJetEta)
        {
            maxJetEta = abs(j.Eta());
            frwdjet   = j;
        }
    }
    for (auto j: jet_lv)
    {
       if (j != frwdjet)
       {
           if ( (j+ frwdjet).M() > maxMjj)
           {
               maxMjj = (j + frwdjet).M();
               max_mjjJet = j;
           }
       }
    }
    if (maxMjj > 0)
    {
        frwdjetEta      = frwdjet.Eta();
        maxmjjJetdEta   = fabs(frwdjet.Eta() - max_mjjJet.Eta());
    }
    return ROOT::RVec<float>({maxMjj,frwdjetEta,maxmjjJetdEta});
}

float getEtaMaxEtaJet(VecF jet_pt, VecF jet_eta, VecF jet_phi, VecF jet_e)
{
    return frwdJetPairVars(jet_pt,jet_eta,jet_phi,jet_e).at(1);
}

float getMaxMjjMaxEtaJet(VecF jet_pt, VecF jet_eta, VecF jet_phi, VecF jet_e)
{
    return frwdJetPairVars(jet_pt,jet_eta,jet_phi,jet_e).at(0);
}

float getdEtaMaxMjjMaxEtaJet(VecF jet_pt, VecF jet_eta, VecF jet_phi, VecF jet_e)
{
    return frwdJetPairVars(jet_pt,jet_eta,jet_phi,jet_e).at(2);
}

/**Return lep_flavour**/
int getLepFlavour(float lep_ID_0, float lep_ID_1, int dilep_type)
{
    int lep_flavour = 9999;
    if (dilep_type) {
        if (fabs(lep_ID_0)==11&&fabs(lep_ID_1)==11) lep_flavour = 0; // ee pairs
        else if (fabs(lep_ID_0)==11&&fabs(lep_ID_1)==13) lep_flavour = 1; // em pairs
        else if (fabs(lep_ID_0)==13&&fabs(lep_ID_1)==11) lep_flavour = 2; // me pairs
        else if (fabs(lep_ID_0)==13&&fabs(lep_ID_1)==13) lep_flavour = 3; // mm pairs
    }
    return lep_flavour;
}
int getChannel(float lep_ID_0, float lep_ID_1, int dilep_type)
{
    int channel = 9999;
    if (dilep_type) {
        if (fabs(lep_ID_0)==11&&fabs(lep_ID_1)==11) channel = 1; // ee pairs
        else if (fabs(lep_ID_0)==13&&fabs(lep_ID_1)==13) channel = 2; // mm pairs
        else if (fabs(lep_ID_0)==13&&fabs(lep_ID_1)==11) channel = 3; // me pairs
        else if (fabs(lep_ID_0)==11&&fabs(lep_ID_1)==13) channel = 3; // em pairs
    }
    return channel;
}




/**Return max_eta**/
float getMaxEta(float lep_Eta_0, float lep_Eta_1, int dilep_type)
{
    float max_eta = 9999;
    if (dilep_type) max_eta = fmax(fabs(lep_Eta_0), fabs(lep_Eta_1));
    return max_eta;
}

/**Return Delta lep_Eta**/
float getDEta(float lep_Eta_0, float lep_Eta_1, int dilep_type)
{
    float DEtall01 = 9999;
    if (dilep_type) DEtall01 = (lep_Eta_0 - lep_Eta_1 );
    return DEtall01;
}
