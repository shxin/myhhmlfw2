import ROOT,sys
import utils

def totWAndXs(inFileList):
   #Open file list and retrieve sum of totalEventsWeighted
   sm = ROOT.TChain("sumWeights")
   fL = open(inFileList,'r')
   
   for f in fL.readlines():
       print f.rstrip()
       sm.AddFile(f.rstrip())
   
   sdf = ROOT.RDataFrame(sm)
   totW    = sdf.Sum("totalEventsWeighted").GetValue()
   #Get Cross-section X k-factor
   dsid    = int(sdf.Max("dsid").GetValue())
   xs      = 0
   if dsid: 
       xs      = utils.getXS(dsid) 
   return (totW,xs)
