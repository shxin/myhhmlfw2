import ROOT

def getXS(dsid):
  if dsid ==0 :
      return 1
  else:
      tdpFile = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/AnalysisTop/TopDataPreparation/XSection-MC16-13TeV.data"
      d       = dict([ (line.split()[0],line.split()[1:]) for line in open(tdpFile) if len(line.split())>0 and line.split()[0] !="#"])
      return float(d[str(dsid)][0])*float(d[str(dsid)][1])


def get_generatorWeights(tree):
    gw = ROOT.map('long',ROOT.vector('double'))()
    for ev in tree:
        genWeight = ROOT.vector('double')()
        [genWeight.push_back(x) for x in ev.mc_generator_weights]
        pair = ROOT.std.pair('long',ROOT.vector('double'))(ev.eventNumber,genWeight)
        gw[ev.eventNumber]  = genWeight
        #gw.insert(gw.begin(),pair)
    return gw

def get_files(fName):
    ##Returns a list of input files from the fName argument
    is_csv = len([x for x in open(fName).read().splitlines() if "," in x])
    fList  = []
    if (is_csv):
        fList = open(fName).read().splitlines()[0].split(",")
    else:
        fList = open(fName).read().splitlines()
    return fList


#def get_generatorWeights(tree,evNum):
#    tree.BuildIndex("eventNumber")
#    if tree.GetEntryWithIndex(evNum) >0:
#        return tree.mc_generator_weights
#    else:
#        return ROOT.vector('double')
#
