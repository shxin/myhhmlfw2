import ROOT,utils,sys,branches,cuts,os
sys.path.insert(0, "python")
#ROOT.ROOT.EnableImplicitMT()
from ROOT import TMVA
from array import array
TMVA = ROOT.TMVA
reader = TMVA.Reader()

nom     = ROOT.TChain("nominal")
#pL      = ROOT.TChain("particleLevel")
smw     = ROOT.TChain("sumWeights")

isMC  = True
fName = sys.argv[1]

inFiles = utils.get_files(sys.argv[1])
for f in inFiles:
    ftmp = ROOT.TFile.Open(f)
#    isMC = ftmp.GetListOfKeys().Contains("particleLevel")
    nom.AddFile(f.rstrip())
    smw.AddFile(f.rstrip())
#    if isMC: 
#        pL.AddFile(f.rstrip())

#if isMC: 
#    nom.AddFriend(pL,"particleLevel")
#nom.BuildIndex("eventNumber")
treeIdx = ROOT.TTreeIndex(nom,"eventNumber","runNumber")
nom.SetTreeIndex(treeIdx)

df      = ROOT.RDataFrame(nom)                       #nominal tree dataframe

totW = 1.0
xs   = 1.0

if isMC:
    if len(sys.argv)>3:
        totW = float(sys.argv[3])
        xs   = float(sys.argv[4])
    else:
        sdf     = ROOT.RDataFrame(smw)
        totW    = sdf.Sum("totalEventsWeighted").GetValue()  #
        dsid    = int(sdf.Max("dsid").GetValue())            # Retieve DSID
        xs      = utils.getXS(dsid)                          # Cross-section x BR in fb

    #Add totalEventWeights and XS  columns to nominal DF
    df  = df.Define("totalEventsWeighted",'float(TPython::Eval("totW"))').Define("xs",'float(TPython::Eval("xs"))')


ROOT.gInterpreter.Declare("""
using VecF_p = const ROOT::RVec<float>&;
using VecF_e = const ROOT::RVec<float>&;

float getMindr(VecF_p phi, VecF_e eta, VecF_e pt, float lep_phi_0, float lep_eta_0, float lep_pt_0, float lep_phi_1, float lep_eta_1, float lep_pt_1, float lep_phi_2, float lep_eta_2, float lep_pt_2, int trilep_type)
{
float minDeltaR_LJ_0     = 9999;
float minDeltaR_LJ_1     = 9999;
float minDeltaR_LJ_2     = 9999;
float DeltaR_min_lep_jet = 9999;
TVector3 jet;
TVector3 lep0;
TVector3 lep1;
TVector3 lep2;
lep0.SetPtEtaPhi(lep_pt_0, lep_eta_0, lep_phi_0);
lep1.SetPtEtaPhi(lep_pt_1, lep_eta_1, lep_phi_1);
lep2.SetPtEtaPhi(lep_pt_2, lep_eta_2, lep_phi_2);
    for (size_t i = 0; i < phi.size(); i++) {
      jet.SetPtEtaPhi(pt[i], eta[i], phi[i]);
      float  DR_LJ_0_tmp  = ROOT::Math::VectorUtil::DeltaR(lep0,jet);
      if (DR_LJ_0_tmp < minDeltaR_LJ_0) {
        minDeltaR_LJ_0 = DR_LJ_0_tmp;
      }

      float  DR_LJ_1_tmp  = ROOT::Math::VectorUtil::DeltaR(lep1,jet);
      if (DR_LJ_1_tmp < minDeltaR_LJ_1) {
        minDeltaR_LJ_1 = DR_LJ_1_tmp;
      }
      float  DR_LJ_2_tmp  = ROOT::Math::VectorUtil::DeltaR(lep2,jet);
      if (DR_LJ_2_tmp < minDeltaR_LJ_2) {
        minDeltaR_LJ_2 = DR_LJ_2_tmp;
      }

    }
      DeltaR_min_lep_jet = minDeltaR_LJ_0;
      if (minDeltaR_LJ_1 < DeltaR_min_lep_jet) DeltaR_min_lep_jet = minDeltaR_LJ_1;
      if (trilep_type) if (minDeltaR_LJ_2 < DeltaR_min_lep_jet) DeltaR_min_lep_jet = minDeltaR_LJ_2;
    return DeltaR_min_lep_jet;
}
""")

ROOT.gInterpreter.Declare("""
using VecF_p = const ROOT::RVec<float>&;
using VecF_e = const ROOT::RVec<float>&;

float getMindr0(VecF_p phi, VecF_e eta, VecF_e pt, float lep_phi_0, float lep_eta_0, float lep_pt_0)
{
float minDeltaR_LJ_0     = 9999;
TVector3 jet;
TVector3 lep;
lep.SetPtEtaPhi(lep_pt_0, lep_eta_0, lep_phi_0);
    for (size_t i = 0; i < phi.size(); i++) {
      jet.SetPtEtaPhi(pt[i], eta[i], phi[i]);
      float  DR_LJ_0_tmp  = ROOT::Math::VectorUtil::DeltaR(lep,jet);
      if (DR_LJ_0_tmp < minDeltaR_LJ_0) {
        minDeltaR_LJ_0 = DR_LJ_0_tmp;
      }

    }
    return minDeltaR_LJ_0;
}
""")
ROOT.gInterpreter.ProcessLine('''
TMVA::Experimental::RReader modeltt("TMVA_tt_BDTG.weights.xml");
computeModeltt = TMVA::Experimental::Compute<10, float>(modeltt);
''')
ROOT.gInterpreter.ProcessLine('''
TMVA::Experimental::RReader modelvj("TMVA_Vjets_BDTG.weights.xml");
computeModelvj = TMVA::Experimental::Compute<9, float>(modelvj);
''')
ROOT.gInterpreter.ProcessLine('''
TMVA::Experimental::RReader modelvv("TMVA_VV_BDTG.weights.xml");
computeModelvv = TMVA::Experimental::Compute<9, float>(modelvv);
''')
ROOT.gInterpreter.ProcessLine('''
TMVA::Experimental::RReader modelall("TMVA_BDTG_all_BDTG.weights.xml");
computeModelall = TMVA::Experimental::Compute<3, float>(modelall);
''')
##Create a filtered dataframe with l2SS selection.
from cuts import cuts
saveDF  = df.Filter(cuts['l2SS']+"||"+cuts['l30tau'])
#+"||"+cuts['l30tau']
#Only request a branch in output tree if the requested branch exists.
#branchVars = [x for x in saveDF.GetColumnNames()]
branchVars = [x.GetName() for x in nom.GetListOfBranches()]
from branches import toSlim
outVars = ROOT.vector('string')()
[outVars.push_back(x) for x in toSlim if x in branchVars]
outVars.push_back("totalEventsWeighted")
outVars.push_back("xs")
saveDF = saveDF.Define("DeltaR_min_lep_jet", "getMindr(jet_phi,jet_eta,jet_pt,lep_Phi_0,lep_Eta_0,lep_Pt_0,lep_Phi_1,lep_Eta_1,lep_Pt_1,lep_Phi_2,lep_Eta_2,lep_Pt_2,trilep_type)").Define("minDeltaR_LJ_0","getMindr0(jet_phi,jet_eta,jet_pt,lep_Phi_0,lep_Eta_0,lep_Pt_0)").Define("minDeltaR_LJ_1","getMindr0(jet_phi,jet_eta,jet_pt,lep_Phi_1,lep_Eta_1,lep_Pt_1)").Define("minDeltaR_LJ_2","getMindr0(jet_phi,jet_eta,jet_pt,lep_Phi_2,lep_Eta_2,lep_Pt_2)")
##TMVA VARIABLES
saveDF = saveDF.Define("abs_lep_Eta_0", "abs(lep_Eta_0)").Define("abs_lep_Eta_1", "abs(lep_Eta_1)")
saveDF = saveDF.Define("dilep_typef","float(dilep_type)")
saveDF = saveDF.Define("nJets_OR_DL1r_85f","float(nJets_OR_DL1r_85)")
saveDF = saveDF.Define("nJets_ORf","float(nJets_OR)")
saveDF = saveDF.Define("BDTG_ttv", ROOT.computeModeltt, ROOT.modeltt.GetVariableNames())
saveDF = saveDF.Define("BDTG_tt","float(BDTG_ttv[0])")
saveDF = saveDF.Define("BDTG_VVv", ROOT.computeModelvv, ROOT.modelvv.GetVariableNames())
saveDF = saveDF.Define("BDTG_VV","float(BDTG_VVv[0])")
saveDF = saveDF.Define("BDTG_Vjetsv", ROOT.computeModelvj, ROOT.modelvj.GetVariableNames())
saveDF = saveDF.Define("BDTG_Vjets","float(BDTG_Vjetsv[0])")
saveDF = saveDF.Define("BDTG_Allv", ROOT.computeModelall, ROOT.modelall.GetVariableNames())
saveDF = saveDF.Define("BDTG_All","float(BDTG_Allv[0])")

outVars.push_back("DeltaR_min_lep_jet")
outVars.push_back("minDeltaR_LJ_0")
outVars.push_back("minDeltaR_LJ_1")
outVars.push_back("minDeltaR_LJ_2")
outVars.push_back("BDTG_tt")
outVars.push_back("BDTG_VV")
outVars.push_back("BDTG_Vjets")
outVars.push_back("BDTG_All")
opts = ROOT.RDF.RSnapshotOptions()
opts.fLazy=True
opts.fMode = "RECREATE"
saveDF.Snapshot('nominal',sys.argv[2],outVars)
