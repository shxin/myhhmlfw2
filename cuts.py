import ROOT

cuts={}
#cuts['l2SS']  = "((RunYear==2015&&(HLT_mu18_mu8noL1||HLT_2e12_lhloose_L12EM10VH||HLT_e17_lhloose_mu14))||(RunYear==2016&&(HLT_mu22_mu8noL1||HLT_2e17_lhvloose_nod0||HLT_e17_lhloose_nod0_mu14))||(RunYear==2017&&(HLT_mu22_mu8noL1||HLT_2e24_lhvloose_nod0||HLT_e17_lhloose_nod0_mu14)))&&(dilep_type&&lep_ID_0*lep_ID_1>0&&lep_isQMisID_1==0&&lep_isQMisID_0==0&&!nTaus_OR_Pt25&&nJets_OR>=4&&nJets_OR_DL1r_70&&lep_Pt_0>20e3&&lep_Pt_1>20e3)&&Mll01>12e3&&(((abs(lep_ID_0)==13&&lep_isMedium_0)||(abs(lep_ID_0)==11&&abs(lep_Eta_0)<2.0))&&((abs(lep_ID_1)==11&&abs(lep_Eta_1)<2.0)||(abs(lep_ID_1)==13&&lep_isMedium_1)))&&(((dilep_type==3||dilep_type==2)&&DRll01>0.5)||(dilep_type==1))&&((abs(lep_ID_0)==13 && lep_isMedium_0 && lep_isolationFCLoose_0 && lep_promptLeptonVeto_TagWeight_0<-0.5) || (abs(lep_ID_0)==11 && lep_isolationFCLoose_0 && lep_isTightLH_0 && lep_chargeIDBDTResult_0>0.7 && lep_ambiguityType_0 == 0 && lep_promptLeptonVeto_TagWeight_0<-0.7))&&((abs(lep_ID_1)==13 && lep_isMedium_1 && lep_isolationFCLoose_1 && lep_promptLeptonVeto_TagWeight_1<-0.5) || (abs(lep_ID_1)==11 && lep_isolationFCLoose_1 && lep_isTightLH_1 && lep_chargeIDBDTResult_1>0.7 && lep_ambiguityType_1 == 0 && lep_promptLeptonVeto_TagWeight_1<-0.7))"

cuts['l2SS'] =  "(dilep_type&&!nTaus_OR_Pt25_RNN&&(((abs(lep_ID_0)==13 && lep_isMedium_0)||(abs(lep_ID_0)==11&& lep_isLooseLH_0)) &&((abs(lep_ID_1)==13 && lep_isMedium_1)||(abs(lep_ID_1)==11 && lep_isLooseLH_1))) && GlobalTrigDecision && abs(total_charge)==2)"


#!nTaus_OR_Pt25&&nJets_OR>=2&&lep_Pt_0>15e3&&lep_Pt_1>15e3)&&(((abs(lep_ID_0)==13&&lep_isMedium_0)||(abs(lep_ID_0)==11&&abs(lep_Eta_0)<2.0))&&((abs(lep_ID_1)==11&&abs(lep_Eta_1)<2.0)||(abs(lep_ID_1)==13&&lep_isMedium_1)))&&((abs(lep_ID_0)==13 && lep_isMedium_0 && lep_isolationFCLoose_0 && lep_promptLeptonVeto_TagWeight_0<-0.5) || (abs(lep_ID_0)==11 && lep_isolationFCLoose_0 && lep_isTightLH_0 && lep_chargeIDBDTResult_0>0.7 && lep_ambiguityType_0 == 0 && lep_promptLeptonVeto_TagWeight_0<-0.7))&&((abs(lep_ID_1)==13 && lep_isMedium_1 && lep_isolationFCLoose_1 && lep_promptLeptonVeto_TagWeight_1<-0.5) || (abs(lep_ID_1)==11 && lep_isolationFCLoose_1 && lep_isTightLH_1 && lep_chargeIDBDTResult_1>0.7 && lep_ambiguityType_1 == 0 && lep_promptLeptonVeto_TagWeight_1<-0.7)))"

cuts['l30tau'] = "(trilep_type&&!nTaus_OR_Pt25_RNN&&abs(total_charge)==1)"

