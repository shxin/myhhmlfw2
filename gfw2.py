import ROOT,utils,sys,branches,cuts,os
sys.path.insert(0, "python")
#ROOT.ROOT.EnableImplicitMT()
ROOT.gInterpreter.Declare('#include <functions.h>')

nom     = ROOT.TChain("nominal")
#pL      = ROOT.TChain("particleLevel")
smw     = ROOT.TChain("sumWeights")

isMC  = True
fName = sys.argv[1]

inFiles = utils.get_files(sys.argv[1])
for f in inFiles:
    ftmp = ROOT.TFile.Open(f)
#    isMC = ftmp.GetListOfKeys().Contains("particleLevel")
    nom.AddFile(f.rstrip())
    smw.AddFile(f.rstrip())
#    if isMC: 
#        pL.AddFile(f.rstrip())

#if isMC: 
#    nom.AddFriend(pL,"particleLevel")
#nom.BuildIndex("eventNumber")
treeIdx = ROOT.TTreeIndex(nom,"eventNumber","runNumber")
nom.SetTreeIndex(treeIdx)

df      = ROOT.RDataFrame(nom)                       #nominal tree dataframe

totW = 1.0
xs   = 1.0

if isMC:
    if len(sys.argv)>3:
        totW = float(sys.argv[3])
        xs   = float(sys.argv[4])
    else:
        sdf     = ROOT.RDataFrame(smw)
        totW    = sdf.Sum("totalEventsWeighted").GetValue()  #
        dsid    = int(sdf.Max("dsid").GetValue())            # Retieve DSID
        xs      = utils.getXS(dsid)                          # Cross-section x BR in fb

    #Add totalEventWeights and XS  columns to nominal DF
    df  = df.Define("totalEventsWeighted",'float(TPython::Eval("totW"))').Define("xs",'float(TPython::Eval("xs"))')

'''
ROOT.gInterpreter.Declare("""
using VecF_p = const ROOT::RVec<float>&;
using VecF_e = const ROOT::RVec<float>&;

float getMindr(VecF_p phi, VecF_e eta, VecF_e pt, float lep_phi_0, float lep_eta_0, float lep_pt_0, float lep_phi_1, float lep_eta_1, float lep_pt_1, float lep_phi_2, float lep_eta_2, float lep_pt_2, int trilep_type)
{
float minDeltaR_LJ_0     = 9999;
float minDeltaR_LJ_1     = 9999;
float minDeltaR_LJ_2     = 9999;
float DeltaR_min_lep_jet = 9999;
TVector3 jet;
TVector3 lep0;
TVector3 lep1;
TVector3 lep2;
lep0.SetPtEtaPhi(lep_pt_0, lep_eta_0, lep_phi_0);
lep1.SetPtEtaPhi(lep_pt_1, lep_eta_1, lep_phi_1);
lep2.SetPtEtaPhi(lep_pt_2, lep_eta_2, lep_phi_2);
    for (size_t i = 0; i < phi.size(); i++) {
      jet.SetPtEtaPhi(pt[i], eta[i], phi[i]);
      float  DR_LJ_0_tmp  = ROOT::Math::VectorUtil::DeltaR(lep0,jet);
      if (DR_LJ_0_tmp < minDeltaR_LJ_0) {
        minDeltaR_LJ_0 = DR_LJ_0_tmp;
      }

      float  DR_LJ_1_tmp  = ROOT::Math::VectorUtil::DeltaR(lep1,jet);
      if (DR_LJ_1_tmp < minDeltaR_LJ_1) {
        minDeltaR_LJ_1 = DR_LJ_1_tmp;
      }
      float  DR_LJ_2_tmp  = ROOT::Math::VectorUtil::DeltaR(lep2,jet);
      if (DR_LJ_2_tmp < minDeltaR_LJ_2) {
        minDeltaR_LJ_2 = DR_LJ_2_tmp;
      }

    }
      DeltaR_min_lep_jet = minDeltaR_LJ_0;
      if (minDeltaR_LJ_1 < DeltaR_min_lep_jet) DeltaR_min_lep_jet = minDeltaR_LJ_1;
      if (trilep_type) if (minDeltaR_LJ_2 < DeltaR_min_lep_jet) DeltaR_min_lep_jet = minDeltaR_LJ_2;
    return DeltaR_min_lep_jet;
}
""")

ROOT.gInterpreter.Declare("""
using VecF_p = const ROOT::RVec<float>&;
using VecF_e = const ROOT::RVec<float>&;

float getMindr0(VecF_p phi, VecF_e eta, VecF_e pt, float lep_phi_0, float lep_eta_0, float lep_pt_0)
{
float minDeltaR_LJ_0     = 9999;
TVector3 jet;
TVector3 lep;
lep.SetPtEtaPhi(lep_pt_0, lep_eta_0, lep_phi_0);
    for (size_t i = 0; i < phi.size(); i++) {
      jet.SetPtEtaPhi(pt[i], eta[i], phi[i]);
      float  DR_LJ_0_tmp  = ROOT::Math::VectorUtil::DeltaR(lep,jet);
      if (DR_LJ_0_tmp < minDeltaR_LJ_0) {
        minDeltaR_LJ_0 = DR_LJ_0_tmp;
      }

    }
    return minDeltaR_LJ_0;
}
""")
'''
##Create a filtered dataframe with l2SS selection.
from cuts import cuts
saveDF  = df.Filter(cuts['l2SS'])
#+"||"+cuts['l30tau']
#Only request a branch in output tree if the requested branch exists.
#branchVars = [x for x in saveDF.GetColumnNames()]
branchVars = [x.GetName() for x in nom.GetListOfBranches()]
from branches import toSlim
outVars = ROOT.vector('string')()
[outVars.push_back(x) for x in toSlim if x in branchVars]
outVars.push_back("totalEventsWeighted")
outVars.push_back("xs")

saveDF = saveDF.Define("DeltaR_min_lep_jet", "getMindr(jet_phi,jet_eta,jet_pt,lep_Phi_0,lep_Eta_0,lep_Pt_0,lep_Phi_1,lep_Eta_1,lep_Pt_1,lep_Phi_2,lep_Eta_2,lep_Pt_2,trilep_type)")
saveDF = saveDF.Define("minDeltaR_LJ_0","getMindr0(jet_phi,jet_eta,jet_pt,lep_Phi_0,lep_Eta_0,lep_Pt_0)")
saveDF = saveDF.Define("minDeltaR_LJ_1","getMindr0(jet_phi,jet_eta,jet_pt,lep_Phi_1,lep_Eta_1,lep_Pt_1)")
saveDF = saveDF.Define("minDeltaR_LJ_2","getMindr0(jet_phi,jet_eta,jet_pt,lep_Phi_2,lep_Eta_2,lep_Pt_2)")
saveDF = saveDF.Define("MLepMet","getMLepMet(lep_E_0,lep_Phi_0,lep_Eta_0,lep_Pt_0,lep_E_1,lep_Phi_1,lep_Eta_1,lep_Pt_1,met_met,met_phi)")
saveDF = saveDF.Define("MtLepMet","getMLepMet(lep_E_0,lep_Phi_0,lep_Eta_0,lep_Pt_0,lep_E_1,lep_Phi_1,lep_Eta_1,lep_Pt_1,met_met,met_phi)")
saveDF = saveDF.Define("mjjMax_frwdJet","getMaxMjjMaxEtaJet(jet_pt, jet_eta, jet_phi, jet_e)")
saveDF = saveDF.Define("eta_frwdjet","getEtaMaxEtaJet(jet_pt, jet_eta, jet_phi, jet_e)")
saveDF = saveDF.Define("dEta_maxMjj_frwdjet","getdEtaMaxMjjMaxEtaJet(jet_pt, jet_eta, jet_phi, jet_e)")
saveDF = saveDF.Define("max_eta","getMaxEta(lep_Eta_0,lep_Eta_1,dilep_type)")
saveDF = saveDF.Define("DEtall01","getDEta(lep_Eta_0,lep_Eta_1,dilep_type)")
outVars.push_back("DeltaR_min_lep_jet")
outVars.push_back("minDeltaR_LJ_0")
outVars.push_back("minDeltaR_LJ_1")
outVars.push_back("minDeltaR_LJ_2")
outVars.push_back("MLepMet")
outVars.push_back("MtLepMet")
outVars.push_back("mjjMax_frwdJet")
outVars.push_back("eta_frwdjet")
outVars.push_back("dEta_maxMjj_frwdjet")
outVars.push_back("max_eta")
outVars.push_back("DEtall01")
opts = ROOT.RDF.RSnapshotOptions()
opts.fLazy=True
opts.fMode = "RECREATE"
saveDF.Snapshot('nominal',sys.argv[2],outVars)
