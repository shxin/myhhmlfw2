import ROOT ,sys
from ROOT import TMVA
from array import array
TMVA = ROOT.TMVA
reader = TMVA.Reader()

def define_variables(df):
    """
    Define the variables which shall be used for training
    """
    return df.Define("lep_Eta_0", "abs(lep_Eta_0)")\
             .Define("lep_Eta_1", "abs(lep_Eta_1)")\
             .Define("Mll01", "Mll01")\
             .Define("DRll01", "DRll01[1]")\
             .Define("HT_lep", "HT_lep")\
             .Define("HT", "HT")\
             .Define("dilep_type", "dilep_type")\
             .Define("nJets_OR_DL1r_85", "nJets_OR_DL1r_85")\
             .Define("nJets_OR", "nJets_OR")


#variables = ["lep_Eta_0", "lep_Eta_1", "Mll01", "DeltaR_min_lep_jet","DRll01","HT_lep","HT","dilep_type","nJets_OR_DL1r_85","nJets_OR"]
ROOT.gInterpreter.Declare("""
using VecF_p = const ROOT::RVec<float>&;
using VecF_e = const ROOT::RVec<float>&;

float getMindr(VecF_p phi, VecF_e eta, VecF_e pt, float lep_phi_0, float lep_eta_0, float lep_pt_0, float lep_phi_1, float lep_eta_1, float lep_pt_1, float lep_phi_2, float lep_eta_2, float lep_pt_2, int trilep_type)
{
float minDeltaR_LJ_0     = 9999;
float minDeltaR_LJ_1     = 9999;
float minDeltaR_LJ_2     = 9999;
float DeltaR_min_lep_jet = 9999;
TVector3 jet;
TVector3 lep0;
TVector3 lep1;
TVector3 lep2;
lep0.SetPtEtaPhi(lep_pt_0, lep_eta_0, lep_phi_0);
lep1.SetPtEtaPhi(lep_pt_1, lep_eta_1, lep_phi_1);
lep2.SetPtEtaPhi(lep_pt_2, lep_eta_2, lep_phi_2);
    for (size_t i = 0; i < phi.size(); i++) {
      jet.SetPtEtaPhi(pt[i], eta[i], phi[i]);
      float  DR_LJ_0_tmp  = ROOT::Math::VectorUtil::DeltaR(lep0,jet);
      if (DR_LJ_0_tmp < minDeltaR_LJ_0) {
        minDeltaR_LJ_0 = DR_LJ_0_tmp;
      }

      float  DR_LJ_1_tmp  = ROOT::Math::VectorUtil::DeltaR(lep1,jet);
      if (DR_LJ_1_tmp < minDeltaR_LJ_1) {
        minDeltaR_LJ_1 = DR_LJ_1_tmp;
      }
      float  DR_LJ_2_tmp  = ROOT::Math::VectorUtil::DeltaR(lep2,jet);
      if (DR_LJ_2_tmp < minDeltaR_LJ_2) {
        minDeltaR_LJ_2 = DR_LJ_2_tmp;
      }

    }
      DeltaR_min_lep_jet = minDeltaR_LJ_0;
      if (minDeltaR_LJ_1 < DeltaR_min_lep_jet) DeltaR_min_lep_jet = minDeltaR_LJ_1;
      if (trilep_type) if (minDeltaR_LJ_2 < DeltaR_min_lep_jet) DeltaR_min_lep_jet = minDeltaR_LJ_2;
    return DeltaR_min_lep_jet;
}
""")

ROOT.gInterpreter.Declare("""
using VecF_p = const ROOT::RVec<float>&;
using VecF_e = const ROOT::RVec<float>&;

float getMindr0(VecF_p phi, VecF_e eta, VecF_e pt, float lep_phi_0, float lep_eta_0, float lep_pt_0)
{
float minDeltaR_LJ_0     = 9999;
TVector3 jet;
TVector3 lep;
lep.SetPtEtaPhi(lep_pt_0, lep_eta_0, lep_phi_0);
    for (size_t i = 0; i < phi.size(); i++) {
      jet.SetPtEtaPhi(pt[i], eta[i], phi[i]);
      float  DR_LJ_0_tmp  = ROOT::Math::VectorUtil::DeltaR(lep,jet);
      if (DR_LJ_0_tmp < minDeltaR_LJ_0) {
        minDeltaR_LJ_0 = DR_LJ_0_tmp;
      }

    }
    return minDeltaR_LJ_0;
}
""")

ROOT.gInterpreter.ProcessLine('''
TMVA::Experimental::RReader model("TMVA_tt_BDTG.weights.xml");
computeModel = TMVA::Experimental::Compute<10, float>(model);
''')

nom     = ROOT.TChain("nominal")
fName = sys.argv[1]
f = ROOT.TFile.Open(sys.argv[1])
nom.AddFile(sys.argv[1])
#reader.AddVariable(str(var),var) for var in variables
df      = ROOT.RDataFrame(nom)                       #nominal tree dataframe
#bdt = TMVA.Experimental.RReader("TMVA_tt_BDTG.weights.xml")
#reader.BookMVA('BDTG Method','TMVA_tt_BDTG.weights.xml')
#variables = bdt.GetVariableNames()
#print variables
df = df.Define("DeltaR_min_lep_jet", "getMindr(jet_phi,jet_eta,jet_pt,lep_Phi_0,lep_Eta_0,lep_Pt_0,lep_Phi_1,lep_Eta_1,lep_Pt_1,lep_Phi_2,lep_Eta_2,lep_Pt_2,trilep_type)").Define("minDeltaR_LJ_0","getMindr0(jet_phi,jet_eta,jet_pt,lep_Phi_0,lep_Eta_0,lep_Pt_0)").Define("minDeltaR_LJ_1","getMindr0(jet_phi,jet_eta,jet_pt,lep_Phi_1,lep_Eta_1,lep_Pt_1)").Define("minDeltaR_LJ_2","getMindr0(jet_phi,jet_eta,jet_pt,lep_Phi_2,lep_Eta_2,lep_Pt_2)")
#df2 = df.Define("y", TMVA.Experimental.RReader.Compute<10, float>(bdt), variables)
df = df.Define("abs_lep_Eta_0", "abs(lep_Eta_0)").Define("abs_lep_Eta_1", "abs(lep_Eta_1)")
df = df.Define("dilep_typef","float(dilep_type)")
df = df.Define("nJets_OR_DL1r_85f","float(nJets_OR_DL1r_85)")
df = df.Define("nJets_ORf","float(nJets_OR)")
df = df.Define("y", ROOT.computeModel, ROOT.model.GetVariableNames())
#df = df.Define('y', ROOT.computeModel, { abs(lep_Eta_0), abs(lep_Eta_1), "Mll01", "DRll01", "HT_lep", "DeltaR_min_lep_jet", "HT", "dilep_type", "nJets_OR_DL1r_85", "nJets_OR" })
c = ROOT.TCanvas()
h = df.Histo1D("y")
h.Draw()
c.SaveAs("asd.png")

